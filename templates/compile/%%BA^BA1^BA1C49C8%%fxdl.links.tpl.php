<?php /* Smarty version 2.6.25, created on 2022-03-22 20:38:14
         compiled from admin/fxdl.links.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<link href="/static/js/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="/static/js/jquery.min.js"></script>
<script type="text/javascript" src="/static/js/clipboard.min.js"></script>
<script type="text/javascript">
    function ajaxSelectChange(id,tag,url){
        if(id<1)return false;
        $.post(
                url,
                {id:id},
                function(data){
                    if(data.str=='success'){
                        //$('.'+tag).append(data.data);
                        $('.'+tag).html(data.data);
                    }else{
                        alert(data.str);
                    }
                },
                'json'
        );
    }
	
			$(function(){
			var clipboard = new ClipboardJS('.btn');
			clipboard.on('success', function(e) {
				alert("复制推广链接成功")  
			});
		
    })
	
</script>

<style type="text/css">
.text-hint.normal{color:#F00;}

</style>

<div style="width:96%;margin:auto;padding:auto;" class="page-content clearfix">
    <div class="clearfix administrator_style" id="administrator">
                <div class="table_menu_list" id="testIframe" style="width: 100%; height: 551px;">
                    <!--<table class="table table-striped table-bordered table-hover" id="sample_table">-->
                    <table id="sample-table-1" class="table table-striped table-bordered table-hover" style="margin-top: 20px;">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>商品名称</th>
                            <th>商品价格</th>
                            <th>分成比例</th>
                            <th>推广链接</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody id="goodsTb">
                        
                        <tr>
                <td>0</td>
                <td>全量推广</td>
                <td><img src="https://api.nbhao.org/v1/qrcode/make?text=<?php echo $this->_tpl_vars['url']; ?>
/?dl=<?php echo $this->_tpl_vars['userinfo']['uid']; ?>
" /></td>
                <td><?php echo $this->_tpl_vars['userinfo']['dl_tcbl']; ?>
%</td>
                <td id="tc_box_wei20"><?php echo $this->_tpl_vars['url']; ?>
/?dl=<?php echo $this->_tpl_vars['userinfo']['uid']; ?>
</td>
                <td><button data-clipboard-action="copy" id="btn" data-clipboard-target="#tc_box_wei20" class="btn btn-xs btn-info wxCopynum3">复制推广链接</button></td>
            </tr>
            <?php $_from = ($this->_tpl_vars['lists']); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
            <tr>
                <td><?php echo $this->_tpl_vars['k']; ?>
</td>
                <td><?php echo $this->_tpl_vars['v']['title']; ?>
</td>
                <td><?php echo $this->_tpl_vars['v']['config']; ?>
</td>
                <td><?php echo $this->_tpl_vars['userinfo']['dl_tcbl']; ?>
%</td>
                <td id="tc_box_wei<?php echo $this->_tpl_vars['k']; ?>
"><?php echo $this->_tpl_vars['url']; ?>
/?ac=<?php echo $this->_tpl_vars['v']['name']; ?>
&dl=<?php echo $this->_tpl_vars['userinfo']['uid']; ?>
</td>
                
                <td>
                <button data-clipboard-action="copy" id="btn<?php echo $this->_tpl_vars['k']; ?>
" data-clipboard-target="#tc_box_wei<?php echo $this->_tpl_vars['k']; ?>
" class="btn btn-xs btn-info wxCopynum3">复制推广链接</button>
                
                </td>
            </tr>
            <?php endforeach; endif; unset($_from); ?>
            </tbody>
                    </table>
                    
                 </div>
                </div>
    </div>
</body>
</html>